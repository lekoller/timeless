import React from 'react';
import renderer from 'react-test-renderer';
import { icons } from 'utils';

import ToolTip from '../index';

describe('ToolTip snapshot test', () => {
  it('renders', () => {
    const tree = renderer
      .create(
        <ToolTip tooltiptext="Testing">
          <img src={icons.warning} onClick={() => {}} alt="warning" />
        </ToolTip>
      )
      .toJSON();

    expect(tree).toMatchSnapshot();
  });
});
