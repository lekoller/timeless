import React from 'react';
import renderer from 'react-test-renderer';
import { background } from 'utils';

import SidePushMenu from '../index';

describe('SidePushMenu Snapshot', () => {
  it('renders', () => {
    const tree = renderer
      .create(
        <SidePushMenu setBackground={() => {}} lockDirection={() => {}} setDirection={() => {}} />
      )
      .toJSON();

    expect(tree).toMatchSnapshot();
  });
});
